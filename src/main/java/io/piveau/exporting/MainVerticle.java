package io.piveau.exporting;

import io.piveau.exporting.hub.ExportingHubVerticle;
import io.piveau.pipe.connector.PipeConnector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Launcher;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        vertx.deployVerticle(ExportingHubVerticle.class, new DeploymentOptions().setWorker(true), ar -> {
            if (ar.succeeded()) {
                PipeConnector.create(vertx, cr -> {
                    if (cr.succeeded()) {
                        cr.result().consumerAddress(ExportingHubVerticle.ADDRESS);
                        startFuture.complete();
                    } else {
                        startFuture.fail(cr.cause());
                    }
                });
            } else {
                startFuture.fail(ar.cause());
            }
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
