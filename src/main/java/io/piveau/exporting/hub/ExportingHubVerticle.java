package io.piveau.exporting.hub;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.connector.PipeContext;
//import io.vertx.circuitbreaker.CircuitBreaker;
//import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Set;
import java.util.stream.Collectors;

public class ExportingHubVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.exporting.hub.queue";

//    private CircuitBreaker breaker;

    private WebClient client;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);

        /*
        breaker = CircuitBreaker.create("hub-breaker", vertx, new CircuitBreakerOptions()
                .setTimeout(20000)
                .setMaxRetries(2))
                .retryPolicy(count -> count * 10000L);
        */

        startFuture.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        ObjectNode dataInfo = pipeContext.getDataInfo();

        if (dataInfo.hasNonNull("content") && dataInfo.get("content").asText().equals("identifierList")) {
            deleteIdentifiers(pipeContext);
        } else {
            exportMetadata(pipeContext);
        }
    }

    private void exportMetadata(PipeContext pipeContext) {
        JsonNode config = pipeContext.getConfig();
        String address = config.path("address").textValue();
        String apiKey = config.path("apiKey").textValue();

        JsonNode dataInfo = pipeContext.getDataInfo();

        String identifier;
        try {
            identifier = URLEncoder.encode(dataInfo.path("identifier").asText(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            pipeContext.setFailure(e);
            return;
        }

        HttpRequest<Buffer> request = client.putAbs(address + "/datasets/" + identifier)
                .putHeader("Authorization", apiKey)
                .putHeader("Content-Type", pipeContext.getDataMimeType())
                .addQueryParam("catalogue", config.path("catalogue").textValue())
                .addQueryParam("hash", dataInfo.path("hash").textValue());

        request.sendBuffer(Buffer.buffer(pipeContext.getStringData(), "UTF-8"), ar -> {
            if (ar.succeeded()) {
                if (ar.result().statusCode() == 201) {
                    pipeContext.log().info("Dataset created: {}", dataInfo);
                } else if (ar.result().statusCode() == 200) {
                    pipeContext.log().info("Dataset updated: {}", dataInfo);
                } else if (ar.result().statusCode() == 304) {
                    pipeContext.log().info("Dataset skipped: {}", dataInfo);
                } else {
                    pipeContext.setFailure(ar.result().statusCode() + " - " + ar.result().statusMessage() + " - " + ar.result().bodyAsString());
                }
            } else {
                pipeContext.setFailure(ar.cause());
            }
        });

        /*
        breaker.<String>execute(fut -> request.sendBuffer(Buffer.buffer(pipeContext.getStringData(), "UTF-8"), ar -> {
            if (ar.succeeded()) {
                if (ar.result().statusCode() == 201) {
                    fut.complete("created");
                } else if (ar.result().statusCode() == 200) {
                    fut.complete("updated");
                } else if (ar.result().statusCode() == 304) {
                    fut.complete("skipped");
                } else {
                    pipeContext.log().error(ar.result().statusMessage() + " - " + ar.result().bodyAsString());
                    fut.complete("error");
                }
            } else {
                fut.fail(ar.cause());
            }
        })).setHandler(ar -> {
            if (ar.succeeded()) {
                pipeContext.log().info("Dataset " + ar.result() + ": " + dataInfo.toString());
                if (!ar.result().equals("error")) {
                    pipeContext.forward(client);
                }
            } else {
                pipeContext.log().error("Request sent error: " +  ar.cause().getMessage());
                pipeContext.setFailure(ar.cause().getMessage());
            }
        });
        */
    }

    private void deleteIdentifiers(PipeContext pipeContext) {
        JsonNode config = pipeContext.getConfig();
        String address = config.path("address").textValue();
        String apiKey = config.path("apiKey").textValue();

        String catalogueId = config.path("catalogue").textValue();
        HttpRequest<Buffer> request = client.getAbs(address + "/datasets")
                .putHeader("Authorization", apiKey)
                .addQueryParam("catalogue", catalogueId)
                .addQueryParam("sourceIds", "true");

        request.send(ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 200) {
                    Set<String> sourceIds = new JsonArray(pipeContext.getStringData()).stream().map(Object::toString).collect(Collectors.toSet());
                    Set<String> targetIds = ar.result().bodyAsJsonArray().stream().map(Object::toString).collect(Collectors.toSet());
                    int targetSize = targetIds.size();
                    targetIds.removeAll(sourceIds);
                    pipeContext.log().info("Source {}, target {}, deleting {} datasets", sourceIds.size(), targetSize, targetIds.size());
                    targetIds.forEach(datasetId -> deleteDataset(pipeContext, datasetId, catalogueId));
                } else {
                    pipeContext.log().error(response.statusMessage());
                }
            } else {
                pipeContext.setFailure(ar.cause());
            }
        });

        /*
        breaker.<Set<String>>execute(fut -> request.send(ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                switch (response.statusCode()) {
                    case 200:
                        Set<String> sourceIds = new JsonArray(pipeContext.getStringData()).stream().map(Object::toString).collect(Collectors.toSet());
                        Set<String> targetIds = ar.result().bodyAsJsonArray().stream().map(Object::toString).collect(Collectors.toSet());
                        targetIds.removeAll(sourceIds);
                        fut.complete(targetIds);
                        break;
                    case 404:
                    default:
                        fut.fail(response.statusMessage());
                }
            } else {
                fut.fail(ar.cause());
            }
        })).setHandler(ar -> {
            if (ar.succeeded()) {
                Set<String> diffIds = ar.result();
                pipeContext.log().info("Start deleting " + diffIds.size() + " datasets");
                diffIds.forEach(dataset -> deleteDataset(pipeContext, dataset));
            } else {
                pipeContext.setFailure(ar.cause().getMessage());
            }
        });
        */
    }

    private void deleteDataset(PipeContext pipeContext, String datasetId, String catalogueId) {
        JsonNode config = pipeContext.getConfig();
        String address = config.path("address").textValue();
        String apiKey = config.path("apiKey").textValue();
        HttpRequest<Buffer> request = client.deleteAbs(address + "/datasets/" + datasetId)
                .putHeader("Authorization", apiKey)
                .addQueryParam("catalogue", catalogueId);
        request.send(ar -> {
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                switch (response.statusCode()) {
                    case 200:
                        pipeContext.log().info("Dataset '{}' deleted", datasetId);
                        break;
                    case 404:
                        pipeContext.log().warn("Dataset '{}' not found", datasetId);
                        break;
                    default:
                        pipeContext.log().error("{} - {} ({})", response.statusCode(), response.statusMessage(), datasetId);
                }
            } else {
                pipeContext.log().error("Delete dataset", ar.cause());
            }
        });
    }

}
