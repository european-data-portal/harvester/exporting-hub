# exporting hub
Microservice for exporting to the hub.

The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/harvester/pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Pipe Configuration](#pipe-configuration)
1. [Data Info Object](#data-info-object)
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [License](#license)

## Pipe Configuration

_mandatory_

* `address`

    Address of the source

* `catalogue`

    The catalogue id the dataset is related to

_optional_

* `apiKey` 

    Key for authorize the API usage

Example
```json
{
  "address": "http://acme.com",
  "apiKey": "",
  "catalogue": "govdata"
}
```

## Data Info Object

* `total`
 
    Total number of datasets (optional)

* `counter` 

    The number of this dataset (optional)

* `identifier` 

    The unique identifier in the source of this dataset (required)

* `hash` 

    The hash value calculated at the source (optional)

Example
```json
{
  "total": 20300,
  "counter": 5,
  "identifier": "dataset_1",
  "hash": ""
}
```

## Build
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/exporting-hub.git
$ cd exporting-hub
$ mvn package
```

## Run

```bash
$ java -jar target/exporting-hub-far.jar
```

## Docker

Build docker image:

```bash
$ docker build -t exporting-hub .
```

Run docker image:

```^bash
$ docker run -it -p 8080:8080 exporting-hub
```

## License

[Apache License, Version 2.0](LICENSE.md)
